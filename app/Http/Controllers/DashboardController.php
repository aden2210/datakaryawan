<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class DashboardController extends Controller
{
    public function show()
    {
        $karyawan = DB::table('karyawan')->get();

        return view('dashboard.gets.index', ['datakaryawan' => $karyawan]);
    }

    public function create(Request $req)
    {
        $nama = $req->nama_karyawan;
        $nomor = $req->no_karyawan;
        $nomor_telpon = $req->no_telp_karyawan;
        $jabatan = $req->jabatan_karyawan;
        $divisi = $req->divisi_karyawan;

        DB::table('karyawan')->insert(
            [
            'nama_karyawan' => $nama,
            'no_karyawan' => $nomor,
            'no_telp_karyawan' => $nomor_telpon,
            'jabatan_karyawan' => $jabatan,
            'divisi_karyawan' => $divisi
            ]
        );

        return redirect ('dashboard/view');
    }

    public function edit(Int $id)
    {
        $edit = DB::table('karyawan')->where('id', $id)->first();
        
        return view('dashboard.posts.edit', ['edit' => $edit]);
    }

    public function update(Request $request, Int $id)
    {
        $update = DB::table('karyawan')->where('id', $id)->update(
            [
            'nama_karyawan' => $request -> nama_karyawan,
            'no_karyawan' => $request -> no_karyawan,
            'no_telp_karyawan' => $request -> no_telp_karyawan,
            'jabatan_karyawan' => $request -> jabatan_karyawan,
            'divisi_karyawan' => $request -> divisi_karyawan
            ]
        );

        return redirect ('dashboard/view');
    }

    public function delete(Int $id)
    {
        $delete = DB::table('karyawan')->where('id', $id)->delete();

        return redirect ('dashboard/view');
    }
}
