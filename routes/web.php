<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [DashboardController::class, 'show']);
Route::get('/dashboard/upload', function () {
    return view('dashboard.posts.index');
});
Route::post('dashboard/simpan', [DashboardController::class, 'create']);
Route::get('/dashboard/view', [DashboardController::class, 'show']);
Route::get('/dashboard/update/{id}', [DashboardController::class, 'edit']);
Route::post('/dashboard/update/{id}', [DashboardController::class, 'update']);
Route::get('/dashboard/delete/{id}', [DashboardController::class, 'delete']);