<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
    <div class="position-sticky pt-3">
      <ul class="nav flex-column">
        <li class="nav-item">
          <a class="nav-link {{ Request::is('dashboard/view') ? 'active' : '' }}" href="/dashboard/view">
            <span data-feather="file"></span>
            Data Karyawan
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{ Request::is('dashboard/upload') ? 'active' : '' }}" href="/dashboard/upload">
            <span data-feather="upload"></span>
            Upload Data
          </a>
        </li>
      </ul>
    </div>
</nav>