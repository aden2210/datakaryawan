@extends('dashboard.layouts.main')
@section('section')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2">Edit Data Karyawan</h1> 
</div>

<form action="{{ url('dashboard/update/' . $edit->id) }}" method="POST">
  {{ csrf_field() }}
  <div class="mb-3">
    <label  class="form-label">Nama Karyawan</label>
    <input type="text" class="form-control" name="nama_karyawan" value="{{ $edit->nama_karyawan }}">
  </div>
  <div class="mb-3">
    <label class="form-label">Nomor Karyawan</label>
    <input type="number" class="form-control" name="no_karyawan" value="{{ $edit->no_karyawan }}">
  </div>
  <div class="mb-3">
    <label class="form-label">Nomor Telpon Karyawan</label>
    <input type="number" class="form-control" name="no_telp_karyawan" value="{{ $edit->no_telp_karyawan }}">
  </div>
  <div class="mb-3">
    <label class="form-label">Jabatan Karyawan</label>
    <input type="text" class="form-control" name="jabatan_karyawan" value="{{ $edit->jabatan_karyawan }}">
  </div>
  <div class="mb-3">
    <label class="form-label">Divisi Karyawan</label>
    <input type="text" class="form-control" name="divisi_karyawan" value="{{ $edit->divisi_karyawan }}">
  </div>
  <button type="submit" class="btn btn-primary" value="ubah">Submit</button>
  <a href="/dashboard/view" class="btn btn-danger" tabindex="-1" role="button" > Cancel</a>
</form>

@endsection