@extends('dashboard.layouts.main')
@section('section')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2">Upload Data Karyawan</h1> 
</div>

<form action="dashboard/simpan" method="POST">
  {{ csrf_field() }}
  <div class="mb-3">
    <label  class="form-label">Nama Karyawan</label>
    <input type="text" class="form-control" name="nama_karyawan">
  </div>
  <div class="mb-3">
    <label class="form-label">Nomor Karyawan</label>
    <input type="number" class="form-control" name="no_karyawan">
  </div>
  <div class="mb-3">
    <label class="form-label">Nomor Telpon Karyawan</label>
    <input type="number" class="form-control" name="no_telp_karyawan">
  </div>
  <div class="mb-3">
    <label class="form-label">Jabatan Karyawan</label>
    <input type="text" class="form-control" name="jabatan_karyawan">
  </div>
  <div class="mb-3">
    <label class="form-label">Divisi Karyawan</label>
    <input type="text" class="form-control" name="divisi_karyawan">
  </div>
  <button type="submit" class="btn btn-primary" value="daftar">Submit</button>
</form>

@endsection