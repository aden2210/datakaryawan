@extends('dashboard.layouts.main')
@section('section')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2">Data Karyawan</h1> 
</div>

<table class="table">
  <thead>
    <tr>
      <th>ID</th>
      <th>Nama Karyawan</th>
      <th>Nomor Karyawan</th>
      <th>Nomor Telpon Karyawan</th>
      <th>Jabatan Karyawan</th>
      <th>Divisi Karyawan</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($datakaryawan as $data)
    <tr>
      <td>{{ $data->id }}</td>
      <td>{{ $data->nama_karyawan }}</td>
      <td>{{ $data->no_karyawan }}</td>
      <td>{{ $data->no_telp_karyawan }}</td>
      <td>{{ $data->jabatan_karyawan }}</td>
      <td>{{ $data->divisi_karyawan }}</td>
      <td>
        <a href="{{ url('dashboard/update/' . $data->id) }}" class="badge bg-success"><span data-feather="edit-3"></span></a> |
        <a href="{{ url('dashboard/delete/' . $data->id) }}" class="badge bg-danger"><span data-feather="trash-2"></span></a>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
@endsection